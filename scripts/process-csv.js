/*
 * This file processes the initial data, converting CSV to JSON
 * It also cleans up unused columns and checks assumptions about the data
 * This resulting file public/data.json can be viewed in the 'explore' page
*/
const _ = require('lodash');
const neatCSV = require('neat-csv');
const fs = require('fs');

function processMeasurement({timestamp, recvalue, calcvalue, excthreshold, excthlimit, deviation}) {
  // test some assumptions
  if (excthreshold !== 'f' || excthlimit !== 'f' || deviation !== '0') {
    console.log('unusual row: static values vary', {excthreshold, excthlimit, deviation});
    throw new Exception('bad assumption');
  }
  if (recvalue !== calcvalue) {
    console.log('unusual row: values mismatch', {recvalue, calcvalue});
    throw new Exception('bad assumption');
  }
  return [parseInt(timestamp), parseFloat(recvalue)];
}
(async () => {
  const csvData = fs.readFileSync('./demoCompressorWeekData.csv');
  const data = await neatCSV(csvData);
  // data := {timestamp,metricid,deviceid,recvalue,calcvalue,excthreshold,excthlimit,deviation}

  // group data by device id
  const dataByDeviceId = _(data).groupBy(({deviceid}) => deviceid).toPairs().map(([k, v]) => ({deviceId: k, metrics: v})).value();

  // group data by metric id and process using `processMeasurement`
  const dataByDeviceIdAndMetricId = _(dataByDeviceId).map(({deviceId, metrics}) =>
    ({
      deviceId,
      metrics: _(metrics).groupBy(({metricid}) => metricid).toPairs().map(([k2, v2]) =>
        ({metricId: k2, measurements: _.map(v2, processMeasurement)})
      ).value()})
  ).value();


  const jsonData = JSON.stringify(dataByDeviceIdAndMetricId);
  fs.writeFileSync('./public/data.json', jsonData);

  console.log(`wrote ${jsonData.length} bytes to public/data.json`);
})();
