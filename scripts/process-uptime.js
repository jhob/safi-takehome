/*
 * This file takes the public/data.json file and summarizes it:
 * - bucket it into 5 minute buckets with avg power and current values
 * - determine the state of the machine from the average values
 * - write into public/uptime.json
 * */
const _ = require('lodash');
const neatCSV = require('neat-csv');
const Statistics = require('statistics.js');
const fs = require('fs');


function calculateUptime(data) {
  const out = []
  for (const {deviceId, metrics} of data) {
    const {measurements: current} = metrics.find(({metricId}) => metricId === "Iavg_A");
    const {measurements: power} = metrics.find(({metricId}) => metricId === "Psum_kW");
    const values = power.map(([ts, v]) => ({ts, v}))
    const stats = new Statistics(values, {ts: 'interval', v: 'metric'})
    const minTime = stats.minimum('ts')
    const maxTime = stats.maximum('ts')
    const rangeMS = stats.range('ts')
    const FIVE_MINUTES = 60 * 1000 * 5
    const IDLE_THRESHOLD = 0.1
    const LOADED_THRESHOLD = 0.20 * stats.range('v')
    const rangeM = Math.ceil(rangeMS / FIVE_MINUTES)
    let currentCursor = 0
    let powerCursor = 0
    const buckets = []
    for (const minute of _.range(rangeM)) {
      const end = minTime + minute * FIVE_MINUTES
      const currentBucket = []
      const powerBucket = []
      for ( currentCursor ; current[currentCursor][0] < end ; currentCursor++ ) currentBucket.push(current[currentCursor][1])
      for ( powerCursor ; power[powerCursor][0] < end ; powerCursor++ ) powerBucket.push(power[powerCursor][1])
      const avgCurrent = currentBucket.length ? _(currentBucket).sum() / currentBucket.length : 0
      const avgPower = powerBucket.length ? _(powerBucket).sum() / powerBucket.length : 0
      buckets.push({
        ts: end,
        avgCurrent,
        avgPower,
        state: powerBucket.length === 0 ? 'off' : avgCurrent < IDLE_THRESHOLD ? 'on/idle' : (avgPower > LOADED_THRESHOLD ? 'on/loaded' : 'on/unloaded')
      })
    }
    out.push({
      deviceId,
      uptime: buckets,
      idleThreshold: IDLE_THRESHOLD,
      loadedThreshold: LOADED_THRESHOLD,
    })
  }
  return out
}

(async () => {
  const rawData = fs.readFileSync('./public/data.json');
  const jsonData = JSON.parse(rawData)
  const uptimeData = JSON.stringify(calculateUptime(jsonData))
  fs.writeFileSync('./public/uptime.json', uptimeData);
  console.log(`wrote ${uptimeData.length} bytes to public/uptime.json`);
})();
