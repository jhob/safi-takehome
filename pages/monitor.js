import Head from 'next/head'
import Link from 'next/link'
import UptimeChart from '../components/UptimeChart'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Safi Takehome</title>
      </Head>

      <main>
        <h1 className="title">
          Uptime Monitor
        </h1>
        <Link href="/"><a>Back</a></Link>
        <UptimeChart />
      </main>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }

        .zoom {
          cursor: move;
          fill: none;
          pointer-events: all;
        }
      `}</style>
    </div>
  )
}
