import React, {useState, useEffect, useMemo, useRef} from 'react'
import * as d3 from 'd3'

import makeUptimeChart from '../helpers/makeUptimeChart'

export default () => {
  const svgRef = useRef(null)
  const [data, setData] = useState([])
  const [selectedDeviceId, setSelectedDeviceId] = useState(null)
  const [isLoading, setLoading] = useState(true)
  useEffect(() => {
    const getData = async () => {
      const response = await fetch('./uptime.json')
      const jsonData = await response.json()
      setData(jsonData)
      setLoading(false)
      const metrics = (data.find(({deviceId}) => deviceId === selectedDeviceId) || data[0] || {metrics: []}).metrics;
      setSelectedDeviceId(jsonData[0].deviceId)
    }
    getData()
  }, [])

  useEffect(() => {
    let values;
    values = data.find(({deviceId}) => deviceId == selectedDeviceId) || {loadedThreshold: 0, idleThreshold: 0, uptime: []};
    makeUptimeChart(svgRef.current, values);
  }, [selectedDeviceId])


  return (
    <div className={`chart ${isLoading ? 'loading' : ''}`}>
      {isLoading ? null : (
        <React.Fragment>
        <svg ref={svgRef} />
      </React.Fragment>
      )}
    </div>
  )
}
