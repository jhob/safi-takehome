import React from 'react'

export default ({data, selectedDeviceId, selectedMetricId, onSelectDevice, onSelectMetric} ) => {
  const metrics = (data.find(({deviceId}) => deviceId === selectedDeviceId) || data[0] || {metrics: []}).metrics;
  return (
    <div className="picker">
      <label>
        Device
        <select onChange={(e) => onSelectDevice(e.target.value)}>
          {data.map(({deviceId, i}) => <option key={i} value={deviceId}>{deviceId}</option>)}
        </select>
      </label>
      <label>
        Metric
        <select onChange={(e) => onSelectMetric(e.target.value)}>
          {metrics.map(({metricId, i}) => <option key={i} value={metricId}>{metricId}</option>)}
        </select>
      </label>
    </div>
  )
}
