import React, {useState, useEffect, useMemo, useRef} from 'react'
import * as d3 from 'd3'

import Picker from './Picker'
import makeChart from '../helpers/makeChart'

export default () => {
  const svgRef = useRef(null)
  const [data, setData] = useState([])
  const [selectedDeviceId, setSelectedDeviceId] = useState(null)
  const [selectedMetricId, setSelectedMetricId] = useState(null)
  const [isLoading, setLoading] = useState(true)
  useEffect(() => {
    const getData = async () => {
      const response = await fetch('./data.json')
      const jsonData = await response.json()
      setData(jsonData)
      setLoading(false)
      const metrics = (data.find(({deviceId}) => deviceId === selectedDeviceId) || data[0] || {metrics: []}).metrics;
      setSelectedDeviceId(jsonData[0].deviceId)
      setSelectedMetricId(jsonData[0].metrics[0].metricId)
    }
    getData()
  }, [])

  useEffect(() => {
    let values;
    try {
      values = data.find(({deviceId}) => deviceId == selectedDeviceId).metrics.find(({metricId}) => metricId === selectedMetricId).measurements;
    } catch (e) {
      values = [];
    }
    makeChart(svgRef.current, values);
  }, [selectedMetricId, selectedDeviceId])


  return (
    <div className={`chart ${isLoading ? 'loading' : ''}`}>
      {isLoading ? null : (
        <React.Fragment>
        <Picker data={data} selectedDeviceId={selectedDeviceId} selectedMetricId={selectedMetricId} onSelectDevice={setSelectedMetricId} onSelectMetric={setSelectedMetricId} />
        <svg ref={svgRef} />
      </React.Fragment>
      )}
    </div>
  )
}
