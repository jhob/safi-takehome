import * as d3 from 'd3'

const stateColorMap = [
  ['off', 'gray'],
  ['on/idle', 'steelblue'],
  ['on/unloaded', 'green'],
  ['on/loaded', 'orange']
]

function getPercentages(data, [minT, maxT]) {
  const filteredData = data.filter(({ts, state}) => minT.getTime() <= ts && ts <= maxT.getTime())
  console.log({minT, maxT, filteredData})
  const states = {}
  filteredData.forEach(({state}) => {
    states[state] = states[state] == null ? 1 : states[state] + 1
  });
  const percents = Object.entries(states).map(([state, count]) =>
    [state, Math.round(100 * count / filteredData.length)]
  )
  return Object.fromEntries(percents);
}
export default function makeChart(el, {loadedThreshold, idleThreshold, uptime: data}) {

  if (typeof window == 'undefined') return;

  const svg = d3.select(el),
      margin = {top: 20, right: 20, bottom: 410, left: 40},
      margin2 = {top: 100, right: 20, bottom: 320, left: 40},
      width = +640 - margin.left - margin.right,
      height = +480 - margin.top - margin.bottom,
      height2 = +480 - margin2.top - margin2.bottom;

  svg
    .attr("width", 640)
    .attr("height", 520)

  svg.selectAll("*").remove()

  var parseDate = (d) => new Date(d)

  var x = d3.scaleTime().range([0, width]),
      x2 = d3.scaleTime().range([0, width]),
      y2 = d3.scaleLinear().range([height2, 0]);

  var xAxis = d3.axisBottom(x),
      xAxis2 = d3.axisBottom(x2)

  var brush = d3.brushX()
      .extent([[0, 0], [width, height2]])
      .on("brush end", brushed);

  var zoom = d3.zoom()
      .scaleExtent([1, Infinity])
      .translateExtent([[0, 0], [width, height]])
      .extent([[0, 0], [width, height]])
      .on("zoom", zoomed);

  var area2 = d3.area()
      .curve(d3.curveMonotoneX)
      .x(function(d) { return x2(d.ts); })
      .y0(height2)
      .y1(function(d) { return y2(d.avgPower); });


    x.domain(d3.extent(data, function(d) { return d.ts; }));
    y2.domain([0, d3.max(data, function(d) { return d.avgPower; })]);
    x2.domain(x.domain());

  const percentEl = svg.append('g').attr('transform', 'translate(40, 200)')

  const statePercents = getPercentages(data, x.domain())
  stateColorMap.forEach(([state, color], i) => {
    percentEl.append('rect')
      .attr('class', 'swatch')
      .attr('fill', color)
      .attr('width', 80)
      .attr('height', 30)
      .attr('x', 100 * i)
    percentEl.append('text')
      .attr('class', `percentage-${color}`)
      .attr('font-size', '14')
      .attr('font-weight', 'bold')
      .attr('dominant-baseline', "middle")
      .attr('text-anchor', "middle")
      .attr('x', 100 * i + 40)
      .attr('y', 15)
      .text(`${statePercents[state] || 0}%`);
    percentEl.append('text')
      .attr('class', `state`)
      .attr('font-size', '14')
      .attr('font-weight', 'bold')
      .attr('dominant-baseline', "middle")
      .attr('text-anchor', "middle")
      .attr('x', 100 * i + 40)
      .attr('y', 48)
      .text(state)
  })

  const defs = svg.append('defs')
  defs.append("clipPath")
      .attr("id", "clip")
    .append("rect")
      .attr("width", width)
      .attr("height", height);

  var focus = svg.append("g")
      .attr("class", "focus")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



  var context = svg.append("g")
      .attr("class", "context")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

  const areas = [];
  stateColorMap.forEach(([state, color], i) => {
    let area = d3.area()
        .curve(d3.curveMonotoneX)
        .x(function(d) { return x(d.ts); })
        .y0(height)
        .y1(function(d) { return d.state == state ? 0 : height; });

      focus.append("path")
        .datum(data)
        .attr("class", `area-${i}`)
        .attr("fill", color)
        .attr("clip-path", "url(#clip)")
        .attr("d", area)
      areas.push(area)
    })

    focus.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    context.append("path")
        .datum(data)
        .attr("class", "area")
        .attr("d", area2);

    context.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2);

    context.append("g")
        .attr("class", "brush")
        .call(brush)
        .call(brush.move, x.range());

    svg.append("rect")
        .attr("class", "zoom")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(zoom);


  function brushed() {
    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
    var s = d3.event.selection || x2.range();
    x.domain(s.map(x2.invert, x2));
    focus.select(".axis--x").call(xAxis);
    svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
        .scale(width / (s[1] - s[0]))
        .translate(-s[0], 0));
    updateAreasAndPercentages(x.domain());
  }

  function zoomed() {
    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
    var t = d3.event.transform;
    x.domain(t.rescaleX(x2).domain());
    focus.select(".axis--x").call(xAxis);
    context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
    updateAreasAndPercentages(x.domain());
  }

  function updateAreasAndPercentages(domain) {
    const statePercents = getPercentages(data, domain)
    stateColorMap.forEach(([state, color], i) => {
      svg.select(`.percentage-${color}`)
        .text(`${statePercents[state] || 0}%`);
    })
    areas.forEach((area, i) => {
      focus.select(`.area-${i}`).attr("d", area);
    })
  }


  function type(d) {
    d.ts = parseDate(d.ts);
    d.avgCurrent = +d.avgCurrent;
    return d;
  }
}
