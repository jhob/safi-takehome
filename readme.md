Safi Takehome
-------------

This repository includes an investigation into some medical equipment electrical
data, per the given file `./demoCompressorWeeekData.csv`.

It includes a two step data processing pipeline as well as a web app with
two visualizations, one broader view of the data provided at 
`/explore` and another uptime specific view at `/monitor`.

Getting started
---------------

Requires `node` and `yarn` or `npm`

Install node dependencies e.g. `yarn`

Drop the file `demoCompressorWeekData.csv` into the project root

Convert csv data to json `node scripts/process-csv.js`

Summarize uptime information from json `node scripts/process-uptime.js`

Run the web application `yarn dev`

Open browser and navigate to `http://localhost:3000`


Development Journey
-------------------

I took a little too big of a bite at first without properly understanding the
data. The 'explore' view gave me a good sense of the data we're looking at but
did little to help refine the actual data.

I made the mistake of using react together with D3. 
D3 was useful as I was able to start with a decent area chart with zoom/brush behavior,
it's implementation does not play well with react's virtual dom. I'd chosen to use
react out of habit, but a framework like Svelte have benefitted in this exercise.
The current workaround depends on `useRef` which is pretty clunky.

I called a stop once the uptime monitor was looking halfway decent but could use
some UX improvements, such as tooltips to suggest what values lie beneath the
area charts.

Next steps would also include DRYing up some of the boilerplate between pages.

Also, I had a question whether ON/IDLE and ON/UNLOADED were mixed up.
There seemed to be a 0.6A threshold at which, but I went with the suggested 0.1A
threshold, which meant there was pretty much no idle time (with the 5 minute
bucket averaging at least).


Feedback on the test
--------------------

The column headers describing the data are technical and various metrics require
interpretation. Having had limited electrical engineering training I surmised the 
I1 was a current reading and P1 was a useful power reading and had started the uptime
dashboard using these.  However, would have been good to have a glossary of terms / reference
material to begin with.
I was given the information I needed in an email exchange but that was long after my first attempt.
For a take-home test I think relevant information should be given up front.

If I were to suggest improvements, I would include more context, as suggested
above, and scope the expectations in a little tighter. This came across as a
pretty expansive data cleaning, UI/UX design as well as full-stack coding challenge,
all of which sucks up the 2-4 hours pretty quickly. I probably spent 6 hours on
this test, more than I felt comfortable with.
